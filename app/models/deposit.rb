class Deposit < ApplicationRecord
  belongs_to :account

  validates_presence_of :value
  validates_presence_of :account
end
