class DepositsController < ApplicationController

  # GET /deposits/new
  def new
    @deposit = Deposit.new
  end

  # POST /deposits
  # POST /deposits.json
  def create
    @deposit = Deposit.new(deposit_params)
    @deposit.account = current_user.account

    account = Account.find_by_id(@deposit.account.id)
    account.balance = account.balance.to_f + @deposit.value.to_f
    account.save

    respond_to do |format|
      if @deposit.save
        format.html { redirect_to transitions_path }
        flash[:success] = "Valor depositado com sucesso"
        format.json { render :show, status: :created, location: @deposit }
      else
        format.html { render :new }
        format.json { render json: @deposit.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def deposit_params
      params.require(:deposit).permit(:account_id, :value)
    end
end
