class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :unhashed_password
      t.string :email_confirmation
      t.string :account_number

      t.timestamps
    end
  end
end
